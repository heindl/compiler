
from numpy import genfromtxt
from sklearn import svm
# from sklearn.naive_bayes import GaussianNB
# import matplotlib.pyplot as plt
# from sklearn.cross_validation import train_test_split
# from sklearn.metrics import confusion_matrix
# from sklearn import svm, grid_search, metrics
from sklearn.externals import joblib

import os.path

#f = open("long.csv")
#f.readline()  # skip the header

# dataset = genfromtxt(open('./occurrences.csv','r'), delimiter=',')[1:]
dataset = genfromtxt(open('./occurrences.csv','r'), delimiter=',')
data = [x[1:] for x in dataset]
target = [x[0] for x in dataset]

# X_train, X_test, y_train, y_test = train_test_split(train, target, random_state=0)
# classifier = svm.SVC(kernel='linear')
classifier = svm.SVC(kernel='linear', probability=True).fit(data, target)
# x_pred = classifier.predict_proba(X_test)
#parameters = {'kernel':('linear', 'rbf'), 'C':[1, 10]}
#grid = grid_search.GridSearchCV(classifier, parameters)
# grid.fit(train, target)
# FIXME: Add scipt to create model directory.

if not os.path.exists("./model"):
    os.makedirs("./model")

joblib.dump(classifier, './model/model.pkl')

# print y_pred

# print "precision:", metrics.precision_score(y_test, y_pred)
# print "recall:", metrics.recall_score(y_test, y_pred)
# print "f1 score:", metrics.f1_score(y_test, y_pred)

# Show confusion matrix in a separate window
# cm = confusion_matrix(y_test, y_pred)
# plt.matshow(cm)
# plt.title('Confusion matrix')
# plt.colorbar()
# plt.ylabel('True label')
# plt.xlabel('Predicted label')
# plt.show()


# clf = svm.SVC(gamma=0.001, C=100.)
# clf = GaussianNB()
#clf.fit(train[:-5], target[:-5])
#y_pred = clf.predict_proba(train[-5:])
#y_pred = clf.predict(train)
#print y_pred
#print target[-5:]

# def plot_confusion_matrix(y_pred, y):
#     plt.imshow(metrics.confusion_matrix(y, y_pred),
#                cmap=plt.cm.binary, interpolation='nearest')
#     plt.colorbar()
#     plt.xlabel('true value')
#     plt.ylabel('predicted value')

# print "classification accuracy:", metrics.accuracy_score(target, y_pred)
# plot_confusion_matrix(y_pred, target)

##########
##########
##########
##########
####### FROM EXAMPLE #########
# from sklearn import svm
# from sklearn import datasets
# clf = svm.SVC()
# iris = datasets.load_iris()
# X, y = iris.data, iris.target
# clf.fit(X, y)
# SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0, degree=3, gamma=0.0,
#   kernel='rbf', max_iter=-1, probability=False, random_state=None,
#   shrinking=True, tol=0.001, verbose=False)

 # import pickle
 # s = pickle.dumps(clf)
 # clf2 = pickle.loads(s)
 # clf2.predict(X[0])

 # y[0]
