from numpy import genfromtxt
from sklearn.externals import joblib

dataset = genfromtxt(open('./request.csv','r'), delimiter=',')
data = [x for x in dataset]

model = joblib.load('./model/model.pkl')
probabilities = model.predict_proba(data)

for i in range(len(model.classes_)):
    print model.classes_[i]
    print probabilities[0][i]
#for i in range(len(probabilities)):
#    print probabilities[i]
