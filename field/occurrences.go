package field

import (
	"strings"

	"github.com/heindl/occur"
)

func listOccurrences() []*occur.Record {

	var er error

	species := strings.Split(readFileContents("./species"), "\n")
	var occurrences []*occur.Record
	for i := range species[:5] {
		if species[i] == "" {
			continue
		}
		c := &occur.Collection{
			SpeciesName: species[i],
			Continent:   "NORTH_AMERICA",
		}
		var list []*occur.Record
		if list, er = c.Compile(); er != nil {
			log.Error(er.Error())
			continue
		}
		log.Info("%d returned for %s.", len(list), species[i])
		occurrences = append(occurrences, list...)
	}

	return occurrences

}
