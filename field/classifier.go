package field

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/heindl/occur"
	"github.com/heindl/terra"
)

func BuildOccurrenceClassifier(daysAgo int) (er error) {

	if store, er = terra.Open("./geostore"); er != nil {
		panic(er.Error())
	}
	defer store.Close()

	// var length int
	// if length, er = store.Length(); er != nil {
	// 	panic(er.Error())
	// }
	// fmt.Println("STORE LENGTH", length)

	// List all occurrences.
	occurrences := listOccurrences()
	if len(occurrences) == 0 {
		log.Warning("No occurrence records returned for building model.")
		return
	}

	// Open file and truncate.
	file, er := os.OpenFile("./occurrences.csv", os.O_TRUNC|os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
	if er != nil {
		log.Error(er.Error())
		return
	}
	defer file.Close()

	for i := range occurrences {
		if er = writeClassificationRow(occurrences[i].TaxonKeyString(), occurrences[i].Latitude, occurrences[i].Longitude, occurrences[i].EventDate[:10], daysAgo, file); er != nil {
			continue
		}
	}

	// TODO: Interface with the underlying LibSVM directly for predictions.
	cmd := exec.Command("python", "./classifier.py")
	output, er := cmd.Output()
	if er != nil {
		log.Error("Error compiling model: %s: %s\n", output, er.Error())
		return
	}
	log.Info("Model compiled: %s\n", output)

	return

}

func ClassifyPoint(latitude, longitude float64, daysAgo int) (list []map[string]interface{}, er error) {

	if store.IsClosed() {
		if store, er = terra.Open("./geostore"); er != nil {
			panic(er.Error())
		}
		defer store.Close()
	}

	file, er := os.OpenFile("./request.csv", os.O_TRUNC|os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
	if er != nil {
		log.Error(er.Error())
		return
	}
	defer file.Close()

	// FIXME: This is temporary until the add current geostore data works.
	// Even now, should check to see if a weather feature for this date already exists.
	var added int
	if added, er = addWeatherToGeostore(time.Now(), latitude, longitude, daysAgo); er != nil {
		log.Error(er.Error())
	}
	if added == 0 {
		log.Error("No occurrences added to geostore, so likely weather wasn't found.")
		return
	}

	if er = writeClassificationRow("", latitude, longitude, time.Now().Format("2006-01-02"), daysAgo, file); er != nil {
		log.Error(er.Error())
		return
	}

	cmd := exec.Command("python", "./classify.py")
	output, er := cmd.Output()
	if er != nil {
		log.Error(er.Error())
		return
	}
	a := strings.Split(string(output), "\n")

	list = []map[string]interface{}{}

	// The output results are given in the format:
	// 123456 // TaxonKey
	// 0.38475 // Probability
	// ...
	placeholder := make(map[string]interface{})
	for i := range a {
		if (i+1)%2 != 0 {
			placeholder["taxonKey"] = a[i]
			if placeholder["speciesName"], er = occur.GetSpeciesName(a[i], ""); er != nil {
				log.Error(er.Error())
			}
		} else {
			if placeholder["likelihood"], er = strconv.ParseFloat(a[i], 64); er != nil {
				log.Error(er.Error())
				return
			}
			list = append(list, placeholder)
			placeholder = make(map[string]interface{})
		}
	}

	return

}

func writeClassificationRow(label string, latitude float64, longitude float64, date string, daysAgo int, file *os.File) (er error) {

	// log.Info("Adding classification row for occurrence %f.", occurrence.TaxonKey)

	var classifiers []*terra.Classifier

	point, er := terra.NewPoint(longitude, latitude)
	if er != nil {
		log.Error("Unable to generate a new point with latitude and longitude.")
		return
	}
	point.SetProperty("recordTitle", label)
	point.SetProperty("recordDate", date)

	for _, key := range []string{"LATITUDE", "LONGITUDE"} {
		classifiers = append(classifiers, &terra.Classifier{
			Key:       key,
			StartDate: date,
		})
	}
	var day, end time.Time
	day, er = time.Parse("2006-01-02", date)
	if er != nil {
		log.Error(er.Error())
		return
	}
	end = day.AddDate(0, 0, -1*daysAgo)
	for {
		for _, key := range []string{"DAY_LENGTH", "TMAX", "TMIN", "PRCP"} {
			classifiers = append(classifiers, &terra.Classifier{
				Key:       key,
				StartDate: day.Format("2006-01-02"),
			})
		}
		if day.Equal(end) {
			break
		}
		day = day.AddDate(0, 0, -1)
	}

	for _, key := range []string{"URBAN_AREA", "NATIONAL_PARK"} {
		classifiers = append(classifiers, &terra.Classifier{
			Key:       key,
			StartDate: date,
			Type:      "BOOL",
		})
	}

	er = store.GetClassifiers(point, classifiers)
	if er != nil && er == terra.ErrClassifierNotFound {
		log.Warning("No model row written for for %f (%f, %f) on %s: No classifier found.", label, longitude, latitude, date)
		er = nil
		return
	}
	if er != nil {
		log.Error("No classification row written for for %f (%f, %f) on %s: %s.", label, longitude, latitude, date, er.Error())
		return
	}

	var buffer bytes.Buffer
	if label != "" {
		if _, er = buffer.WriteString(label); er != nil {
			log.Error("No classification row written for for %f (%f, %f) on %s: %s.", label, longitude, latitude, date, er.Error())
			return
		}
	}
	for i := range classifiers {
		if _, er = buffer.WriteString(fmt.Sprintf(",%f", classifiers[i].Value)); er != nil {
			log.Error("No classification row written for for %f (%f, %f) on %s: %s.", label, longitude, latitude, date, er.Error())
			return
		}
	}
	if _, er = buffer.WriteString("\r\n"); er != nil {
		log.Error("No classification row written for for %f (%f, %f) on %s: %s.", label, longitude, latitude, date, er.Error())
		return
	}

	if _, er = buffer.WriteTo(file); er != nil {
		log.Error("No classification row written for for %f (%f, %f) on %s: %s.", label, longitude, latitude, date, er.Error())
		return
	}

	return

}
