package field

import (
	"net/http"
	"strconv"
	"sync"

	"github.com/heindl/terra"
	"github.com/ant0ine/go-json-rest/rest"
)

var serverClassifierDaysAgo int

func Serve(daysAgo int) (er error) {

	serverClassifierDaysAgo = daysAgo

	if store.IsClosed() {
		if store, er = terra.Open("./geostore"); er != nil {
			panic(er.Error())
		}
		defer store.Close()
	}

	handler := rest.ResourceHandler{
		EnableRelaxedContentType: true,
	}
	er = handler.SetRoutes(
		&rest.Route{"GET", "/classify", getPredictions},
	)
	if er != nil {
		log.Error(er.Error())
		return
	}
	if er = http.ListenAndServe(":8080", &handler); er != nil {
		log.Error(er.Error())
	}

	return

}

var lock = sync.RWMutex{}

func getPredictions(w rest.ResponseWriter, r *rest.Request) {

	var er error
	if r.FormValue("latitude") == "" || r.FormValue("latitude") == "0" || r.FormValue("longitude") == "" || r.FormValue("longitude") == "0" {
		rest.NotFound(w, r)
		return
	}

	var latitude, longitude float64

	if latitude, er = strconv.ParseFloat(r.FormValue("latitude"), 64); er != nil {
		rest.Error(w, er.Error(), 500)
		return
	}

	if longitude, er = strconv.ParseFloat(r.FormValue("longitude"), 64); er != nil {
		rest.Error(w, er.Error(), 500)
		return
	}

	lock.RLock()
	list, er := ClassifyPoint(latitude, longitude, serverClassifierDaysAgo)
	lock.RUnlock()

	if er != nil {
		rest.Error(w, er.Error(), 500)
		return
	}
	// Fixme: Map length
	if len(list) == 0 {
		rest.NotFound(w, r)
		return
	}

	w.WriteJson(list)
}
