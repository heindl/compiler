package field

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/kellydunn/golang-geo"

	"github.com/heindl/occur"
	"github.com/heindl/raincollector"
	"github.com/heindl/raincollector/weather"
	"github.com/heindl/terra"
)

var store *terra.Geostore

func BuildHistoricalGeostore(daysAgo int) (er error) {

	// Rebuild the list of weather stations, in case they've changed. Which is unlikely, but I'm unable to find another way to verify.

	// rc := &raincollector.Collection{
	// 	NOAATokens:           []string{"obQsBXJTeHMDQzATdAKlqVEzmJdEHuKb", "ShIkuLOiEkiXPpgYmPGtElQfnwnlNeGu", "EOxIHqnnVTcNldpXhuJvTKuZMzDIBCxE"},
	// 	OpenWeatherMapTokens: []string{"6cb41207f7222c585aa9bfd257b3f3cf"},
	// }
	//
	// if er = rc.DownloadStations(); er != nil {
	// 	return
	// }
	// fmt.Println("Stations compiled.")

	// Open and clear the cache.
	if store, er = terra.Open("./geostore"); er != nil {
		panic(er.Error())
	}
	defer store.Close()
	store.Clear()

	// List all occurrences.
	initialOccurrences := listOccurrences()
	if len(initialOccurrences) == 0 {
		log.Warning("No occurrence records returned for building model.")
		return
	}

	// Add necessary weather data to geostore. Remove occurrences that are missing weather data.
	occurrences := []*occur.Record{}
	for i := range initialOccurrences {
		day, er := time.Parse("2006-01-02", initialOccurrences[i].EventDate[:10])
		if er != nil {
			log.Error(er.Error())
			continue
		}

		added, er := addWeatherToGeostore(day, initialOccurrences[i].Latitude, initialOccurrences[i].Longitude, daysAgo)
		if er == nil && added > 0 {
			occurrences = append(occurrences, initialOccurrences[i])
		}
	}

	// Add urban area features to Geostore.
	urbanAreas, er := terra.NewFeatureCollectionFromJSON([]byte(readFileContents("./geojson/urbanareas.json")))
	if er != nil {
		log.Error(er.Error())
		return
	}
	if _, er = store.Add(urbanAreas...); er != nil {
		log.Error(er.Error())
		return
	}

	// Add national parks feature to Geostore.
	parks, er := terra.NewFeatureCollectionFromJSON([]byte(readFileContents("./geojson/parks.json")))
	if er != nil {
		log.Error(er.Error())
		return
	}
	if _, er = store.Add(parks...); er != nil {
		log.Error(er.Error())
		return
	}

	// Do not continue if nothing was added to the store.
	storeLength, er := store.Length()
	if er != nil {
		log.Error(er.Error())
		return
	}
	if storeLength == 0 {
		log.Warning("Ending script because the geostore contains no features.")
		return
	}

	return
}

func BuildCurrentGeostore(dateRange int) (er error) {

	// Open and clear the cache.
	if store, er = terra.Open("./geostore"); er != nil {
		panic(er.Error())
	}
	defer store.Close()

	storeLength, er := store.Length()
	if er != nil {
		log.Error(er.Error())
		return
	}
	fmt.Println("Initial STORE LENGTH", storeLength)
	store.Clear()

	stations, er := raincollector.GetCurrentStationList()
	if er != nil {
		panic(er.Error())
	}
	fmt.Println("LENGTH OF STATIONS", len(stations))

	for i := range stations {
		if _, er = addWeatherToGeostore(time.Now(), stations[i].Latitude, stations[i].Longitude, dateRange); er != nil {
			log.Error("%s: %s", stations[i].ID, er.Error())
			continue
		}
	}

	// Add urban area features to Geostore.
	urbanAreas, er := terra.NewFeatureCollectionFromJSON([]byte(readFileContents("./geojson/urbanareas.json")))
	if er != nil {
		log.Error(er.Error())
		return
	}
	if _, er = store.Add(urbanAreas...); er != nil {
		log.Error(er.Error())
		return
	}

	// Add national parks feature to Geostore.
	parks, er := terra.NewFeatureCollectionFromJSON([]byte(readFileContents("./geojson/parks.json")))
	if er != nil {
		log.Error(er.Error())
		return
	}
	if _, er = store.Add(parks...); er != nil {
		log.Error(er.Error())
		return
	}

	// Do not continue if nothing was added to the store.
	storeLength, er = store.Length()
	if er != nil {
		log.Error(er.Error())
		return
	}
	fmt.Println("FINAL STORE LENGTH", storeLength)
	if storeLength == 0 {
		log.Warning("Ending script because the geostore contains no features.")
		return
	}

	return
}

func addWeatherToGeostore(day time.Time, latitude float64, longitude float64, daysAgo int) (stationFeaturesAdded int, er error) {

	rc := &raincollector.Collection{
		NOAATokens:           []string{"obQsBXJTeHMDQzATdAKlqVEzmJdEHuKb", "ShIkuLOiEkiXPpgYmPGtElQfnwnlNeGu", "EOxIHqnnVTcNldpXhuJvTKuZMzDIBCxE"},
		OpenWeatherMapTokens: []string{"6cb41207f7222c585aa9bfd257b3f3cf"},
		StartDate:            day.AddDate(0, 0, -1*daysAgo),
		EndDate:              day,
		Latitude:             latitude,
		Longitude:            longitude,
	}
	var weather []*weather.Type
	weather, er = rc.Collect()
	if er != nil {
		log.Error("While compiling weather for (%f, %f) on %s: %s.", longitude, latitude, day.Format("2006-01-02"), er.Error())
		return
	}
	if len(weather) == 0 {
		log.Error("Missing weather values for (%f, %f) on %s.", longitude, latitude, day.Format("2006-01-02"))
		// log.Error("While compiling weather for (%f, %f) on %s: %s.", longitude, latitude, day.Format("2006-01-02"), er.Error())
		return
	}

	stations := make(map[string]*terra.Feature)
	for _, w := range weather {
		if _, ok := stations[w.Station]; !ok {
			stations[w.Station], er = weatherStationToFeature(w)
			if er != nil {
				delete(stations, w.Station)
				continue
			}
		}
		c := &terra.Classifier{
			Key:       w.Key,
			Value:     w.Value,
			StartDate: w.Date,
		}
		stations[w.Station].SetClassifier(c)
	}

	// Create new feature witheach.
	var stationFeatures []*terra.Feature
	for _, feat := range stations {
		stationFeatures = append(stationFeatures, feat)
	}

	stationFeaturesAdded = len(stationFeatures)
	if stationFeaturesAdded == 0 {
		log.Error("While compiling weather for (%f, %f) on %s: %s.", longitude, latitude, day.Format("2006-01-02"), er.Error())
	}

	if _, er = store.Add(stationFeatures...); er != nil {
		log.Error(er.Error())
	}

	return
}

func weatherStationToFeature(weather *weather.Type) (feature *terra.Feature, er error) {

	if weather.Latitude == 0 || weather.Longitude == 0 || weather.Radius == 0 {
		er = fmt.Errorf("Missing variables for weatherStationToFeature: %f, %f, %f", weather.Latitude, weather.Longitude, weather.Radius)
		log.Warning(er.Error())
		return
	}

	point := geo.NewPoint(weather.Latitude, weather.Longitude)

	coords := make([][][]float64, 1)
	var i float64
	for i = 0; i <= 360; i = (i + 10) {
		// Fixme: Added an extra five kilometers because the distance to nearest station is broken.
		circle := point.PointAtDistanceAndBearing(weather.Radius+5, i)
		coords[0] = append(coords[0], []float64{circle.Lng(), circle.Lat()})
	}

	if feature, er = terra.NewPolygon(coords); er != nil {
		log.Error(er.Error())
		return
	}

	return
}

func readFileContents(path string) (response string) {

	var (
		er   error
		file *os.File
		body []byte
	)

	if file, er = os.Open(path); er != nil {
		panic(er)
	}
	defer file.Close()

	if body, er = ioutil.ReadAll(file); er != nil {
		panic(er)
	}

	response = string(body)

	return

}
