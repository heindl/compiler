package field

import (
	"fmt"
	"testing"
)

func catchError(er error, t *testing.T) {
	if er != nil {
		t.Errorf(er.Error())
		panic(er.Error())
	}
}

func TestBuildModel(t *testing.T) {

	list, er := classify(1, 1)
	catchError(er, t)
	fmt.Println(list)

	return

}
