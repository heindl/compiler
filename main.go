package main

import (
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/heindl/compiler/field"
)

func main() {

	var er error

	// build := flag.String("build", "", "data store or model build process")
	// daysAgo := flag.Int("classifier-range", 14, "data store or model build process")
	// serve := flag.Bool("run-server", false, "launch the prediction json rest server")
	// flag.Parse()
	//
	// fmt.Println("VALUE OF BUILD", *build)

	var daysAgo int
	if len(os.Args) == 3 {
		if daysAgo, er = strconv.Atoi(os.Args[2]); er != nil {
			fmt.Errorf("Error parsing classifier time length, which should be the second argument in the array: %v", er.Error())
			return
		}
	} else {
		daysAgo = 7
	}

	switch {
	case os.Args[1] == "build-historical-geostore":
		if er = field.BuildHistoricalGeostore(daysAgo); er != nil {
			fmt.Errorf(er.Error())
			return
		}
		return
	case os.Args[1] == "build-current-geostore":
		if er = field.BuildCurrentGeostore(daysAgo); er != nil {
			fmt.Errorf(er.Error())
			return
		}
		return
	case os.Args[1] == "build-occurrence-classifier":
		if er = field.BuildOccurrenceClassifier(daysAgo); er != nil {
			fmt.Errorf(er.Error())
			return
		}
		return
	case os.Args[1] == "launch-server":
		if er = field.Serve(daysAgo); er != nil {
			fmt.Errorf(er.Error())
			return
		}
		return
	}

	return

}
